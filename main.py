from fastapi import FastAPI, Response
from fastapi.responses import JSONResponse
import uvicorn
import joblib
import string
import pandas as pd
from typing import List

def encode_prenom(prenom: str) -> pd.Series:
    prenom = prenom.lower()
    alphabet = string.ascii_lowercase
    return pd.Series([letter in prenom for letter in alphabet]).astype(int)

reg_log = joblib.load('model.v1.bin')

app = FastAPI()

@app.get("/predict")
async def get_prenom_sexe(prenom: str) -> List:
    """Renvoi le sexe du prenom entrée

    Args:
        prenom (str) : le prenom de l'individu

    Returns:
        list[dict] : prédiction
    """
    if prenom:
        pred = reg_log.predict([encode_prenom(prenom)])

        gender = "Homme" if pred.tolist()[0]==0 else "Femme"
        content = {
            'name': prenom,
            'gender': gender
        }
    
        return JSONResponse(content=content)
    else:
        return JSONResponse(content=[{"error": "You need to pass name as GET parameter."}])
