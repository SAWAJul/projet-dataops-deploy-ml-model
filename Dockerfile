FROM python:3.8
RUN curl -sSL https://install.python-poetry.org | python3 -
RUN mkdir /code
WORKDIR /code
ENV POETRY_VIRTUALENVS_CREATE=false
COPY poetry.lock poetry.lock
COPY pyproject.toml pyproject.toml
RUN $HOME/.local/bin/poetry install
COPY . .
EXPOSE 5000
VOLUME /app/logs
CMD ["uvicorn", "main:app", "--host", "0.0.0.0", "--port", "5000"]